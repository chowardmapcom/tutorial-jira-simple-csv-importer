package com.example.plugins.jira.csvimport.po;

import com.atlassian.jira.plugins.importer.po.common.AbstractImporterWizardPage;
import com.atlassian.jira.plugins.importer.po.common.ImporterProjectsMappingsPage;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SimpleCsvSetupPage extends AbstractImporterWizardPage {

    @FindBy(id = "filePath")
    WebElement filePath;


    public SimpleCsvSetupPage setFilePath(String filePath) {
        this.filePath.sendKeys(filePath);
        return this;
    }

    @Override
    public String getUrl() {
        return "/secure/admin/views/SimpleCsvSetupPage!default.jspa?externalSystem=" +
                "com.example.plugins.tutorial.jira.simple-csv-importer:SimpleCSVImporterKey";
    }

    public ImporterProjectsMappingsPage next() {
        Assert.assertTrue(nextButton.isEnabled());
        nextButton.click();
        return pageBinder.bind(ImporterProjectsMappingsPage.class);
    }
}
